# Collection of `.gitlab-ci.yml` templates

This is a collection of [`.gitlab-ci.yml`](https://docs.gitlab.com/ee/ci/yaml/README.html) file templates.

Files can be included in your `.gitlab-ci.yml` using the `include` keyword:

```yaml
include:
  - project: 'MaxIV/gitlab-ci-templates'
    file: 'Taranta.gitlab-ci.yml'
```

See <https://docs.gitlab.com/ee/ci/yaml/#include> for more information.

## Linting

The script `validate_templates.py` is run in CI to validate the templates using GitLab API.
The API endpoints requires to be authenticated.
The pipeline uses the `CI_LINT_TOKEN` variable. The token is valid for one year.
If the pipeline starts failing, create a new token and add it to the CI/CD variables.

## Available templates

### `Docker.gitlab-ci.yml`

Template to build docker images:

* On tag, publish the image with `latest` and the git tag
* On push, publish the image with the `branch` name as tag

### `PreCommit.gitlab-ci.yml`

Template to run `pre-commit` (when `.pre-commit-config.yaml` exists).

### `PythonPublic.gitlab-ci.yml`

Template to build and publish a Python package to PyPI.
Pytest is also run if tests are found.

The following variables can be used:

* `PYTEST_EXTRA_ARGS` to pass extra arguments to pytest
* `EXTRA_DEB_PACKAGES` to install extra debian packages required to run the tests
* `TEST_WITH_TANGO_DB` can be set to `true` to run a tango database during the tests - by default no db available

### `Taranta.gitlab-ci.yml`

Template to:

* build taranta docker images
* trigger internal gitlab-ci pipeline at MAX IV for Taranta deployment
