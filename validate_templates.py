#!/usr/bin/env python3
# Script to lint gitlab-ci files

import glob
import sys
import requests
import os

CI_PROJECT_ID = os.getenv("CI_PROJECT_ID")
CI_LINT_TOKEN = os.getenv("CI_LINT_TOKEN")
GITLAB_CI_LINTER_URI = f"https://gitlab.com/api/v4/projects/{CI_PROJECT_ID}/ci/lint"


def validate(filename):
    with open(filename) as f:
        content = f.read()
    headers = {"Authorization": f"Bearer {CI_LINT_TOKEN}"}
    r = requests.post(GITLAB_CI_LINTER_URI, headers=headers, data={"content": content})
    if r.json().get("valid"):
        print(f"valid: {filename}")
    else:
        print(f"invalid: {filename}")
        print(r.json())
        sys.exit(1)


def main():
    if len(sys.argv) > 1:
        filenames = sys.argv[1:]
    else:
        filenames = glob.glob("*.gitlab-ci.yml")
    for filename in filenames:
        validate(filename)


if __name__ == "__main__":
    main()
